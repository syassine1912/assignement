package ma.octo.assignement.domain.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public enum EventType {

  VIREMENT("virement"),
  VERSEMENT("Versement d'argent");
  @Getter
  @Setter
  private String type;

}
