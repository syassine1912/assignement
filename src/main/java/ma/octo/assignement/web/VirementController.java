package ma.octo.assignement.web;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.IVirementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("virements")
@RequiredArgsConstructor
class VirementController {

    private final IVirementService virementService;


    @GetMapping("lister_virements")
    List<Virement> loadAll() {
        return virementService.loadAll();
    }


    @PostMapping("/executerVirements")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity createTransaction(@RequestBody VirementDto virementDto){
        try {
            virementService.createTransaction(virementDto);
        } catch (SoldeDisponibleInsuffisantException e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Solde insuffisant",HttpStatus.FORBIDDEN);
        } catch (CompteNonExistantException e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Compte non existant",HttpStatus.FORBIDDEN);
        } catch (TransactionException e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Erreur de transaction",HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity("Transaction effectuée",HttpStatus.CREATED);
    }

}
