package ma.octo.assignement.web;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.service.ICompteService;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController(value = "/comptes")
@RequiredArgsConstructor
public class CompteController {

    private final ICompteService compteService;

    @GetMapping("lister_comptes")
    List<Compte> loadAllCompte() {
        return compteService.loadAllCompte();
    }
}
