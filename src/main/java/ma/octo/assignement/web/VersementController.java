package ma.octo.assignement.web;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.IVersementService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("versements")
@RequiredArgsConstructor
public class VersementController{

    private final IVersementService versementService;

    @GetMapping("lister_versements")
    public List<Versement> loadAll() {
        return  versementService.loadAll();
    }

    @PostMapping("/executerVersements")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity createTransaction(@RequestBody VersementDto versementDto){
        try {
            versementService.createTransaction(versementDto);
        } catch (CompteNonExistantException e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Compte non existant",HttpStatus.FORBIDDEN);
        } catch (TransactionException e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Erreur de transaction",HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity("Transaction effectuée",HttpStatus.CREATED);
    }


}
