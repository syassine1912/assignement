package ma.octo.assignement.service.impl;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.AuditVirement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditVirementRepository;
import ma.octo.assignement.service.IAuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
@Qualifier("AuditVirementServiceImp")
public class AuditVirementServiceImp implements IAuditService {

    Logger LOGGER = LoggerFactory.getLogger(AuditVirementServiceImp.class);

    private final AuditVirementRepository auditVirementRepository;

    @Override
    public void audit(String message, EventType eventType) {

        LOGGER.info("Audit de l'événement {}", eventType);

        AuditVirement audit = new AuditVirement();
        audit.setEventType(eventType);
        audit.setMessage(message);
        auditVirementRepository.save(audit);

    }

}
