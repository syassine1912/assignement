package ma.octo.assignement.service.impl;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.AuditVersement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditVersementRepository;
import ma.octo.assignement.service.IAuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
@Qualifier("AuditVersementServiceImp")
public class AuditVersementServiceImp implements IAuditService {
    Logger LOGGER = LoggerFactory.getLogger(AuditVirementServiceImp.class);

    private final AuditVersementRepository auditVersementRepository;

    @Override
    public void audit(String message, EventType eventType) {

        LOGGER.info("Audit de l'événement {}", eventType);

        AuditVersement audit = new AuditVersement();
        audit.setEventType(eventType);
        audit.setMessage(message);
        auditVersementRepository.save(audit);

    }

}
