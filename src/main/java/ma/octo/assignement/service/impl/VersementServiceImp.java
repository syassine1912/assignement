package ma.octo.assignement.service.impl;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.IVersementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class VersementServiceImp implements IVersementService {

    public static final int MONTANT_MAXIMAL = 10000;
    Logger LOGGER = LoggerFactory.getLogger(VersementServiceImp.class);

    private final VersementRepository versementRepository;
    private final CompteRepository compteRepository;

    @Autowired
    @Qualifier("AuditVersementServiceImp")
    private final IAuditService auditVersementServiceImp;

    @Override
    public List<Versement> loadAll() {
        List<Versement> all = versementRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }

    @Override
    public void createTransaction(VersementDto versementDto) throws CompteNonExistantException, TransactionException {
        Compte beneficiaire = compteRepository.findByRib(versementDto.getRib());

        if (beneficiaire == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (versementDto.getMontantVirement().equals(null)) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVirement().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVirement().intValue() < 10) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (versementDto.getMontantVirement().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if (versementDto.getMotifVersement().length() < 0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }
        if(versementDto.getNomEmetteur().equals("") || versementDto.getNomEmetteur() == null){
            System.out.println("Nom de l'emetteur est vide");
            throw new TransactionException("Nom de l'emetteur est vide");
        }
        if(versementDto.getPrenomEmetteur().equals("") || versementDto.getPrenomEmetteur() == null){
            System.out.println("Prènom de l'emetteur est vide");
            throw new TransactionException("Prènom de l'emetteur est vide");
        }

        if (versementDto.getMontantVirement().intValue() == 0) {
            LOGGER.error("Montant insuffisant");
        }

        beneficiaire.setSolde(new BigDecimal(beneficiaire.getSolde().intValue() + versementDto.getMontantVirement().intValue()));

        compteRepository.save(beneficiaire);

        Versement versement = new Versement();
        versement.setDateExecution(versementDto.getDateExecution());
        versement.setCompteBeneficiaire(beneficiaire);
        versement.setNomEmetteur(versementDto.getNomEmetteur());
        versement.setPrenomEmetteur(versementDto.getPrenomEmetteur());
        versement.setMotifVersement(versementDto.getMotifVersement());
        versement.setMontantVirement(versementDto.getMontantVirement());

        save(versement);

        auditVersementServiceImp.audit("Versement effectué par " + versementDto.getPrenomEmetteur() +" "+ versement.getNomEmetteur() + " vers " + beneficiaire.getNrCompte() + " d'un montant de " + versementDto.getMontantVirement()
                .toString(), EventType.VERSEMENT);
        }

    private void save(Versement versement) {
        versementRepository.save(versement);
    }

}
