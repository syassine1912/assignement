package ma.octo.assignement.service.impl;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.IVirementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class VirementServiceImp implements IVirementService {

    public static final int MONTANT_MAXIMAL = 10000;
    Logger LOGGER = LoggerFactory.getLogger(VirementServiceImp.class);

    private final VirementRepository virementRepository;
    private final CompteRepository compteRepository;

    @Qualifier("AuditVirementServiceImp")
    private final IAuditService auditVirementServiceImp;

    @Override
    public List<Virement> loadAll() {
        List<Virement> all = virementRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }

    @Override
    public void createTransaction(VirementDto virementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

        Compte emetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte beneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

        if (emetteur == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (beneficiaire == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (virementDto.getMontantVirement().equals(null)) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (virementDto.getMontantVirement().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (virementDto.getMontantVirement().intValue() < 10) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (virementDto.getMontantVirement().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if (virementDto.getMotif().length() <= 0 || virementDto.getMotif() == null) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if(virementDto.getNrCompteEmetteur() == beneficiaire.getNrCompte()){
            System.out.println("Transaction Invalide: Même numéro de compte");
            throw new TransactionException("Operation Invalide: Même numéro de compte");
        }

        if (emetteur.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }


        emetteur.setSolde(emetteur.getSolde().subtract(virementDto.getMontantVirement()));
        compteRepository.save(emetteur);

        beneficiaire.setSolde(new BigDecimal(beneficiaire.getSolde().intValue() + virementDto.getMontantVirement().intValue()));
        compteRepository.save(beneficiaire);

        Virement virement = new Virement();
        virement.setDateExecution(virementDto.getDate());
        virement.setCompteBeneficiaire(beneficiaire);
        virement.setCompteEmetteur(emetteur);
        virement.setMontantVirement(virementDto.getMontantVirement());
        virement.setMotifVirement(virementDto.getMotif());

        save(virement);

        auditVirementServiceImp.audit("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                .toString(), EventType.VIREMENT);
    }

    private void save(Virement Virement) {
        virementRepository.save(Virement);
    }


}
