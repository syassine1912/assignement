package ma.octo.assignement.service;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
@Service
public interface IVirementService  {
     List<Virement> loadAll();
     void createTransaction(@RequestBody VirementDto virementDto) throws  SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;
}
