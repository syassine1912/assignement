package ma.octo.assignement.dto;

import lombok.*;
import ma.octo.assignement.domain.Compte;

import java.math.BigDecimal;
import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VersementDto {
    private String nomEmetteur;
    private String prenomEmetteur;
    private String motifVersement;
    private BigDecimal montantVirement;
    private Date dateExecution;
    private String rib;
}
