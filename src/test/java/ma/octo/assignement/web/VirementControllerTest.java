package ma.octo.assignement.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.IVirementService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class VirementControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @MockBean
    IVirementService virementService;

    VirementDto virementDto;

    @BeforeEach
    void setUp() {
         virementDto = new VirementDto("010000A000001000","010000B025001000",
                "tofriend", BigDecimal.valueOf(1000), Calendar.getInstance().getTime());
    }

    @Test
    void loadAll() throws Exception {
        //given
        List<Virement> virementList = new ArrayList<>();
        Virement virement1 = new Virement();
        Virement virement2 = new Virement();
        virement1.setMontantVirement(BigDecimal.valueOf(100));
        virement1.setMotifVirement("motif1");
        virement2.setMontantVirement(BigDecimal.valueOf(200));
        virement2.setMotifVirement("motif2");

        virementList.add(virement1);
        virementList.add(virement2);


        // when
        when(virementService.loadAll()).thenReturn(virementList);

        // then
        mockMvc.perform(get("/virements/lister_virements")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].montantVirement").value(BigDecimal.valueOf(100)))
                .andExpect(jsonPath("$[0].motifVirement").value("motif1"))
                .andExpect(jsonPath("$[1].montantVirement").value(BigDecimal.valueOf(200)))
                .andExpect((jsonPath("$[1].motifVirement").value("motif2")));
    }

    @Test
    @DisplayName("Normal Transaction Creation")
    void canCreateTransaction() throws Exception {
        //given
        String virementJson = this.mapper.writeValueAsString(virementDto);
        //when
        doNothing().when(virementService).createTransaction(any(VirementDto.class));
        //then
        mockMvc.perform(post("/virements/executerVirements")
                .contentType(MediaType.APPLICATION_JSON)
                .content(virementJson)
        ).andExpect(status().isCreated());
    }

    @Test
    @DisplayName("Solde insuffisant Execption")
    void cannotCreateTransactionDueToSoldeDisponibleInsuffisantException() throws Exception {

        //given
        String virementJson = this.mapper.writeValueAsString(virementDto);

        //when
        doThrow(new SoldeDisponibleInsuffisantException()).when(virementService).createTransaction(any(VirementDto.class));

        //then
        mockMvc.perform(post("/virements/executerVirements")
                .contentType(MediaType.APPLICATION_JSON)
                .content(virementJson)
        ).andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("Compte non existant Execption")
    void cannotCreateTransactionDueToSoldeCompteNonExistantException() throws Exception {

        //given
        String virementJson = this.mapper.writeValueAsString(virementDto);

        //when
        doThrow(new CompteNonExistantException()).when(virementService).createTransaction(any(VirementDto.class));

        //then
        mockMvc.perform(post("/virements/executerVirements")
                .contentType(MediaType.APPLICATION_JSON)
                .content(virementJson)
        ).andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("Erreur de transaction")
    void cannotCreateTransactionDueToTransactionException() throws Exception {

        //given
        String virementJson = this.mapper.writeValueAsString(virementDto);

        //when
        doThrow(new TransactionException()).when(virementService).createTransaction(any(VirementDto.class));

        //then
        mockMvc.perform(post("/virements/executerVirements")
                .contentType(MediaType.APPLICATION_JSON)
                .content(virementJson)
        ).andExpect(status().isForbidden());
    }
}