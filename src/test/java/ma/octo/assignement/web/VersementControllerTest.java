package ma.octo.assignement.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.IVersementService;
import ma.octo.assignement.service.IVirementService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class VersementControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @MockBean
    IVersementService versementService;

    VersementDto versementDto;

    String versementJson;

    @BeforeEach
    void setUp() {
        versementDto = new VersementDto("yassine","souissi",
                "tofriend", BigDecimal.valueOf(1000), Calendar.getInstance().getTime(),"RIB1");
        try {
             versementJson = this.mapper.writeValueAsString(versementDto);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }


    @Test
    void loadAll() throws Exception {
        //given
        List<Versement> versementList = new ArrayList<>();
        Versement versement = new Versement();
        versement.setMontantVirement(BigDecimal.valueOf(100));
        versement.setNomEmetteur("Souissi");
        versement.setPrenomEmetteur("Yassine");
        versementList.add(versement);

        // when
        when(versementService.loadAll()).thenReturn(versementList);

        // then
        mockMvc.perform(get("/versements/lister_versements")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].montantVirement").value(BigDecimal.valueOf(100)))
                .andExpect(jsonPath("$[0].nomEmetteur").value("Souissi"))
                .andExpect(jsonPath("$[0].prenomEmetteur").value("Yassine"));
    }


    @Test
    @DisplayName("Normal Transaction Creation")
    void canCreateTransaction() throws Exception {
        //when
        doNothing().when(versementService).createTransaction(any(VersementDto.class));
        //then
        mockMvc.perform(post("/versements/executerVersements")
                .contentType(MediaType.APPLICATION_JSON)
                .content(versementJson)
        ).andExpect(status().isCreated());
    }


    @Test
    @DisplayName("Compte non existant Execption")
    void cannotCreateTransactionDueToSoldeCompteNonExistantException() throws Exception {

        //given
        String virementJson = this.mapper.writeValueAsString(versementDto);

        //when
        doThrow(new CompteNonExistantException()).when(versementService).createTransaction(any(VersementDto.class));

        //then
        mockMvc.perform(post("/versements/executerVersements")
                .contentType(MediaType.APPLICATION_JSON)
                .content(virementJson)
        ).andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("Erreur de transaction")
    void cannotCreateTransactionDueToTransactionException() throws Exception {

        //given
        String virementJson = this.mapper.writeValueAsString(versementDto);

        //when
        doThrow(new TransactionException()).when(versementService).createTransaction(any(VersementDto.class));

        //then
        mockMvc.perform(post("/versements/executerVersements")
                .contentType(MediaType.APPLICATION_JSON)
                .content(virementJson)
        ).andExpect(status().isForbidden());
    }
}