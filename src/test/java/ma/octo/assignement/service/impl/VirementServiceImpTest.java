package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.IAuditService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Calendar;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VirementServiceImpTest {
    private VirementServiceImp virementServiceImp;
    @Mock
    private VirementRepository virementRepository;
    @Mock
    private CompteRepository compteRepository;
    @Mock
    private IAuditService auditService;

    private Compte emetteur;
    private Compte beneficiare;
    private VirementDto virementDto;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        virementServiceImp = new VirementServiceImp(virementRepository,compteRepository,auditService);
        virementDto = new VirementDto("010000A000001000","010000B025001000",
                "tofriend", BigDecimal.valueOf(1000), Calendar.getInstance().getTime());
        emetteur = new Compte();
        beneficiare = new Compte();
        emetteur.setNrCompte("010000A000001000");
        emetteur.setSolde(BigDecimal.valueOf(100000));
        beneficiare.setNrCompte("010000B025001000");
        beneficiare.setSolde(BigDecimal.valueOf(100000));

    }

    private void whenCompteRepositoryIsCalled() {
        when(compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur())).thenReturn(emetteur);
        when(compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire())).thenReturn(beneficiare);
    }

    @Test
    void loadAll() {
        //when
        virementServiceImp.loadAll();
        //then
        verify(virementRepository).findAll();
    }

    @Test
    void canCreateTransaction() throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {
        //when
        when(compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur())).thenReturn(emetteur);
        when(compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire())).thenReturn(beneficiare);
        when(compteRepository.save(emetteur)).thenReturn(emetteur);
        when(compteRepository.save(beneficiare)).thenReturn(beneficiare);
        virementServiceImp.createTransaction(virementDto);

        //then
        verify(virementRepository).save(any());
    }

    @Test
    @DisplayName("Solde insuffisant pour l'utilisateur")
    void cannotCreateTransactionDueToSoldeDisponibleInsuffisantException() {
        //given
        emetteur.setSolde(BigDecimal.valueOf(10));
        //when
        whenCompteRepositoryIsCalled();
        //then
        assertThrows(SoldeDisponibleInsuffisantException.class,()->virementServiceImp.createTransaction(virementDto));
    }


    @Test
    void cannotCreateTransactionDueToCompteNonExistantException() {
        //when
        when(compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur())).thenReturn(null);
        when(compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire())).thenReturn(null);
        //then
        assertThrows(CompteNonExistantException.class,()-> virementServiceImp.createTransaction(virementDto));
    }

    @Test
    @DisplayName("Même numéro de compte")
    void cannotCreateTransactionDueToTransactionExceptionOfSameAccountNumber() {
        //given
        beneficiare.setNrCompte("010000A000001000");
        virementDto.setNrCompteEmetteur("010000A000001000");
        //when
        whenCompteRepositoryIsCalled();
        //then
        assertThrows(TransactionException.class,()-> virementServiceImp.createTransaction(virementDto));
    }

    @Test
    @DisplayName("Montant Vide")
    void cannotCreateTransactionDueToTransactionExceptionMontantVide() {
        //given
        virementDto.setMontantVirement(BigDecimal.valueOf(0));
        //when
        whenCompteRepositoryIsCalled();
        //then
        assertThrows(TransactionException.class,()-> virementServiceImp.createTransaction(virementDto));
    }

    @Test
    @DisplayName("Montant minimal de virement non atteint")
    void cannotCreateTransactionDueToTransactionExceptionMontantMinimal() {
        //given
        virementDto.setMontantVirement(BigDecimal.valueOf(9));
        //when
        whenCompteRepositoryIsCalled();
        //then
        assertThrows(TransactionException.class,()-> virementServiceImp.createTransaction(virementDto));
    }

    @Test
    @DisplayName("Montant maximal de virement dépassé")
    void cannotCreateTransactionDueToTransactionExceptionMontantMaximal() {
        //given
        virementDto.setMontantVirement(BigDecimal.valueOf(20000));
        //when
        whenCompteRepositoryIsCalled();
        //then
        assertThrows(TransactionException.class,()-> virementServiceImp.createTransaction(virementDto));
    }
}