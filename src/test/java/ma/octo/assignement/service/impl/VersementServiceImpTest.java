package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.IAuditService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VersementServiceImpTest {

    private VersementServiceImp versementServiceImp;
    @Mock
    private VersementRepository versementRepository;
    @Mock
    private CompteRepository compteRepository;
    @Mock
    private IAuditService auditService;

    private Compte beneficiare;
    private VersementDto versementDto;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        versementServiceImp = new VersementServiceImp(versementRepository,compteRepository,auditService);
        versementDto = new VersementDto("yassine","souissi",
                "tofriend", BigDecimal.valueOf(1000), Calendar.getInstance().getTime(),"RIB1");
        beneficiare = new Compte();
        beneficiare.setNrCompte("010000B025001000");
        beneficiare.setSolde(BigDecimal.valueOf(100000));
        beneficiare.setRib("RIB1");
    }

    private void whenCompteRepositoryIsCalled() {
        when(compteRepository.findByRib(versementDto.getRib())).thenReturn(beneficiare);
    }

    @Test
    void loadAll() {
        //when
        versementServiceImp.loadAll();
        //then
        verify(versementRepository).findAll();
    }

    @Test
    void canCreateTransaction() throws TransactionException, CompteNonExistantException {
        //when
        whenCompteRepositoryIsCalled();
        when(compteRepository.save(beneficiare)).thenReturn(beneficiare);
        versementServiceImp.createTransaction(versementDto);
        //then
        verify(versementRepository).save(any(Versement.class));
    }

    @Test
    void cannotCreateTransactionDueToCompteNonExistantException() {
        //when
        when(compteRepository.findByRib(versementDto.getRib())).thenReturn(null);
        when(compteRepository.findByRib(versementDto.getRib())).thenReturn(null);
        //then
        assertThrows(CompteNonExistantException.class,()-> versementServiceImp.createTransaction(versementDto));
    }


    @Test
    @DisplayName("Montant Vide")
    void cannotCreateTransactionDueToTransactionExceptionMontantVide() {
        //given
        versementDto.setMontantVirement(BigDecimal.valueOf(0));
        //when
        whenCompteRepositoryIsCalled();
        //then
        assertThrows(TransactionException.class,()-> versementServiceImp.createTransaction(versementDto));
    }

    @Test
    @DisplayName("Montant minimal de virement non atteint")
    void cannotCreateTransactionDueToTransactionExceptionMontantMinimal() {
        //given
        versementDto.setMontantVirement(BigDecimal.valueOf(9));
        //when
        whenCompteRepositoryIsCalled();
        //then
        assertThrows(TransactionException.class,()-> versementServiceImp.createTransaction(versementDto));
    }

    @Test
    @DisplayName("Montant maximal de virement dépassé")
    void cannotCreateTransactionDueToTransactionExceptionMontantMaximal() {
        //given
        versementDto.setMontantVirement(BigDecimal.valueOf(20000));
        //when
        whenCompteRepositoryIsCalled();
        //then
        assertThrows(TransactionException.class,()-> versementServiceImp.createTransaction(versementDto));
    }
}